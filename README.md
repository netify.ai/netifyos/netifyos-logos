# NetifyOS Logos and Graphics

## Description

This project contains logos and graphics for the NetifyOS project.

## Version Branches

The master branch is not used in the NetifyOS project.  Instead, the version branches track the current stable development:

* netifyos7
* netifyos8
* and beyond
