Name: netifyos-logos
Summary: NetifyOS icons and pictures
Version: 70.0.6 
Release: 3%{?dist}
Group: System Environment/Base
URL: https://www.netify.ai/resources/netifyos
Vendor: Netify by eGloo
Packager: Netify by eGloo
Source0: netifyos-logos-%{version}.tar.gz
License: Copyright © 2018 eGloo Inc.  All rights reserved.
BuildArch: noarch
Obsoletes: gnome-logos
Obsoletes: fedora-logos <= 16.0.2-2
Obsoletes: centos-logos
Obsoletes: clearos-logos
Obsoletes: redhat-logos
Provides: gnome-logos = %{version}-%{release}
Provides: system-logos = %{version}-%{release}
Provides: centos-logos = %{version}-%{release}
Provides: redhat-logos = %{version}-%{release}
Provides: system-backgrounds-gnome
Conflicts: kdebase <= 3.1.5
Conflicts: anaconda-images <= 10
Conflicts: redhat-artwork <= 5.0.5

%description
The netifyos-logos package (the "Package") contains files created by the
NetifyOS Project to replace the Red Hat "Shadow Man" logo and  RPM logo.
The Red Hat "Shadow Man" logo, RPM, and the RPM logo are trademarks or
registered trademarks of Red Hat, Inc.

The Package and NetifyOS logos (the "Marks") can only used as outlined
in the included COPYING file. Please see that file for information on
copying and redistribution of the NetifyOS Marks.

%prep
%setup -q

%build

%install
mkdir -p $RPM_BUILD_ROOT/boot/grub
install -p -m 644 -D bootloader/splash.xpm.gz $RPM_BUILD_ROOT/boot/grub/splash.xpm.gz

mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps
for i in pixmaps/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/pixmaps
done

mkdir -p $RPM_BUILD_ROOT%{_datadir}/plymouth/themes/charge
for i in plymouth/charge/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/plymouth/themes/charge
done

(cd anaconda; make DESTDIR=$RPM_BUILD_ROOT install)

%files
%doc COPYING
%{_datadir}/plymouth/themes/charge/
%{_datadir}/pixmaps/*
%{_datadir}/anaconda/boot/splash.lss
%{_datadir}/anaconda/boot/syslinux-splash.png
%{_datadir}/anaconda/pixmaps/*

%dir %{_datadir}/anaconda
%dir %{_datadir}/anaconda/pixmaps
%dir %{_datadir}/anaconda/boot/
%dir %{_datadir}/plymouth/
%dir %{_datadir}/plymouth/themes/
/boot/grub/splash.xpm.gz

%changelog
* Thu Mar 29 2018 Netify <team@netify.ai> - 70.0.6-3.nos7
- Import logo structure
